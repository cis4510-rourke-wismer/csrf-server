/// This is an example for csrf attacks and defences
use actix_files::Files;
use actix_web::{http::header, middleware, web, App, HttpResponse, HttpServer};

mod auth;
mod config;
mod db;
mod handlers;
mod models;
mod templating;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    std::env::set_var("RUST_LOG", "info");

    // start http server
    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Compress::default())
            .wrap(
                middleware::DefaultHeaders::new().header(header::X_CONTENT_TYPE_OPTIONS, "nosniff"),
            )
            // Removes trailing slash in the URL to make is so I don't need as many services
            // Remove duplicate slashes in the URL
            .wrap(middleware::NormalizePath::new(
                middleware::normalize::TrailingSlash::Trim,
            ))
            // enable logging
            .wrap(middleware::Logger::default())
            // Home page
            .service(
                web::resource("/")
                    .wrap(auth::middleware::AuthCheckService::disallow_auth("/zone"))
                    .route(web::get().to(handlers::home)),
            )
            // Home page for logged in users
            .service(
                web::scope("/zone")
                    .wrap(auth::middleware::AuthCheckService::require_auth())
                    .service(web::resource("").route(web::get().to(handlers::zone))),
            )
            .service(
                web::scope("/example")
                    .wrap(auth::middleware::AuthCheckService::require_auth())
                    .service(
                        web::resource("/protected")
                            .route(web::get().to(handlers::examples::get_example_protected))
                            .route(web::post().to(handlers::examples::example_post)),
                    )
                    .service(
                        web::resource("/unprotected")
                            .route(web::get().to(handlers::examples::get_example_unprotected))
                            .route(web::post().to(handlers::examples::example_post)),
                    ),
            )
            // Login related pages
            .service(
                web::resource("/login")
                    .wrap(auth::middleware::AuthCheckService::disallow_auth("/zone"))
                    .route(web::get().to(handlers::auth::login))
                    .route(web::post().to(handlers::auth::login_post)),
            )
            .service(
                web::resource("/register")
                    .wrap(auth::middleware::AuthCheckService::disallow_auth("/zone"))
                    .route(web::get().to(handlers::user::register_get))
                    .route(web::post().to(handlers::user::register_post)),
            )
            .service(web::resource("/logout").route(web::get().to(handlers::auth::logout)))
            // Favicon handler so that it doesn't try to render it.
            .service(web::resource("/favicon.ico").to(|| HttpResponse::NotFound()))
            // Any top level pages, the URL matches the template name
            .service(web::resource("/{page}").route(web::get().to(handlers::page)))
            // Static resources
            .service(Files::new("/static", "static/"))
            // 404 handler
            .default_service(web::route().to(handlers::p404))
    })
    .bind(format!(
        "{}:{}",
        config::SERVER_ADDR.as_str(),
        config::SERVER_PORT.as_str()
    ))?
    .run()
    .await
}
