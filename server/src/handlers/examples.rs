/// Module for endpoints related CSRF vulnerability examples.
use crate::auth::csrf::{check_csrf, csrf_cookie, generate_csrf_token};
use crate::context;
use crate::models::{ServiceError, User};
use crate::templating::{render, render_message};

use actix_http::httpmessage::HttpMessage;
use actix_web::{web::Form, web::Query, Error, HttpRequest, HttpResponse, Result};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct CSRFParams {
    csrf: String,
}

/// get the example page, CSRF protected.
pub async fn get_example_protected(
    req: HttpRequest,
    user: User,
    info: Query<CSRFParams>,
) -> Result<HttpResponse, Error> {
    // check csrf from params
    match req
        .cookies()
        .map_err(|e| {
            ServiceError::general(
                &req,
                &format!("Error getting cookies from request: {}", e),
                false,
            )
        })?
        .iter()
        .find(|c| c.name() == "csrf")
        .map(|c| c.value().to_string())
    {
        Some(cookie) => match cookie == info.csrf {
            true => {}
            false => Err(ServiceError::unauthorized(
                &req,
                "CSRF tokens don't match.",
                true,
            ))?,
        },
        None => Err(ServiceError::unauthorized(
            &req,
            "No CSRF cookies found.",
            true,
        ))?,
    }
    let csrf_token =
        generate_csrf_token().map_err(|s| ServiceError::general(&req, s.message, false))?;
    log::info!("Sending back profile information.");
    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .cookie(csrf_cookie(&csrf_token))
        .body(render(
            "example.html",
            req.uri().path().to_string(),
            Some(context! { "csrf" => &csrf_token, "protection" => "protected" }),
            Some(user),
        )?))
}

/// Get the example page, not CSRF protected
pub async fn get_example_unprotected(req: HttpRequest, user: User) -> Result<HttpResponse, Error> {
    log::info!("Sending back profile information.");
    Ok(HttpResponse::Ok().content_type("text/html").body(render(
        "example.html",
        req.uri().path().to_string(),
        Some(context! { "protection" => "unprotected" }),
        Some(user),
    )?))
}

/// Struct for holding the form parameters for the example form
#[derive(Serialize, Deserialize)]
pub struct ExampleFormParams {
    account: String,
    amount: f32,
    csrf: Option<String>,
}

/// Accepts the example post request for a transfer
pub async fn example_post(
    req: HttpRequest,
    params: Form<ExampleFormParams>,
    user: User,
) -> Result<HttpResponse, ServiceError> {
    // Only check CSRF if it's protected
    if req.path() == "/example/protected" {
        check_csrf(params.csrf.clone(), &req).await?;
    }
    log::info!("Sent money transfer");

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body(render_message(
            "Transfer Sent!",
            "Transfer Sent!",
            format!(
                "A transfer of ${} was successfully sent to account: {}",
                params.amount, params.account
            )
            .as_str(),
            req.uri().path().to_string(),
            Some(user),
        )?))
}
