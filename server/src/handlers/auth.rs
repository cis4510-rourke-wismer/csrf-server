/// Module that handles all the authentication related endpoints in the website
use crate::auth::credentials::{
    credential_validator_username, validate_password_rules, validate_username_rules,
};
use crate::auth::session::{generate_session_token, get_session_token};
use crate::db::session::delete_session;
use crate::models::ServiceError;
use crate::templating::render;

use actix_http::cookie::Cookie;
use actix_web::http::header;
use actix_web::web::Form;
use actix_web::{Error, HttpRequest, HttpResponse, Result};
use log::{info, warn};
use serde::{Deserialize, Serialize};

/// Serves the login page
pub async fn login(req: HttpRequest) -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok().content_type("text/html").body(render(
        "login.html",
        req.uri().path().to_string(),
        None,
        None,
    )?))
}

/// Form parameters for the login form.
#[derive(Serialize, Deserialize)]
pub struct LoginParams {
    identifier: String,
    password: String,
    persist: Option<bool>,
    protected: Option<bool>,
}

/// Handler for the login post request
pub async fn login_post(
    req: HttpRequest,
    params: Form<LoginParams>,
) -> Result<HttpResponse, ServiceError> {
    // Check the username is valid
    if validate_username_rules(&params.identifier).is_err() {
        return Err(ServiceError::bad_request(&req, "Invalid Username", true));
    }
    // Check the password is valid
    if let Err(e) = validate_password_rules(&params.password, &params.password) {
        return Err(e.bad_request(&req));
    }
    match credential_validator_username(&params.identifier, &params.password)
        .await
        .map_err(|s| s.general(&req))?
    {
        Some(user) => {
            let cookie = generate_session_token(
                &user.user_id,
                params.persist.unwrap_or(false),
                params.protected.unwrap_or(false),
            )
            .await
            .map_err(|s| ServiceError::general(&req, s.message, false))?;

            info!("Successfully logged in user: {}", params.identifier);
            Ok(HttpResponse::SeeOther()
                .header(header::LOCATION, "/zone")
                .cookie(cookie)
                .finish())
        }
        None => {
            info!("Invalid credentials: {}", &params.identifier);
            Err(ServiceError::unauthorized(
                &req,
                "Invalid credentials.",
                true,
            ))
        }
    }
}

/// Logout request handler
pub async fn logout(req: HttpRequest) -> Result<HttpResponse, ServiceError> {
    let token = get_session_token(&req);
    match token {
        Some(t) => {
            delete_session(&t).await.map_err(|s| s.general(&req))?;
            info!("Successfully logged out user");
        }
        None => {
            warn!("Token not found in request to log out");
        }
    }
    Ok(HttpResponse::SeeOther()
        .header(header::LOCATION, "/")
        .del_cookie(&Cookie::named("session"))
        .del_cookie(&Cookie::named("csrf"))
        .finish())
}
