db.createCollection("users", {})
db.createCollection("sessions", {})

db.sessions.createIndex( { "expiry": 1 }, { expireAfterSeconds: 1 } )

db.sessions.createIndex( { "token": 1 }, { unique: true } )
db.users.createIndex( { "user_id": 1 }, { unique: true } )
db.users.createIndex( { "username": 1 }, { unique: true } )
