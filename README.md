# CIS 4510 CSRF Demo Server

This repository contains an open-source web server which demonstrates CSRF vulnerabilities and
mitigations.
It is a companion software project to the final paper submitted by Keefer Rourke and Isaac Wismer.

## Building Server

The server is written in rust and compiled with cargo.
To install rustup, the rust toolchain manager, follow the instructions here: <https://www.rust-lang.org/learn/get-started> (or through your operating system's package manager).

Once rustup has been installed, set your toolchain to the latest stable version.

Run the commands:
```
rustup install stable
rustup default stable
```

The server can then be built with the command `cargo build` from within the `server` directory.

## Running the server

To store user credentials, we use a MongoDB database.
This is most easily set up with Docker.
Install docker according to the instructions found at: <https://www.docker.com/get-started> (or through your operating system's package manager).

Then run the commands:

```
docker pull mongo
docker run -d --net="host" --name="csrf" \
    -v [absolute-path-to-project]/server/init/:/docker-entrypoint-initdb.d/ \
    -e MONGO_INITDB_DATABASE=csrf-example mongo
cargo run
```

Ensure you replace the path to the project.

Then navigate to `127.0.0.1:8080` in your browser (localhost won't work as cookies won't be set correctly).

## Docker-compose

The server is published as a docker container at `https://docker.io/krourke/cis4510`.

We provide a `docker-compose.yml` file with this repository which configures and deploys the server according to the experiment environment outlined in our paper.

To start this server and database containers, simply run `docker-compose up -d -f server/docker-compose.yml`.
